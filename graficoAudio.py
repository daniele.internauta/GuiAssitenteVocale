import sys
from PySide2.QtWidgets import QWidget
import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.ticker as ticker
import queue
import numpy as np
import sounddevice as sd
from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Slot , Qt



class MplCanvas(FigureCanvas):
	def __init__(self, parent=None, width=50.26, height=10.5, dpi=96,  facecolor=(0, 0, 0, 0)):
		fig = Figure(figsize=(width, height), dpi=dpi, facecolor=(0, 0, 0, 0))
		self.axes = fig.add_subplot(111)
		super(MplCanvas, self).__init__(fig)
		fig.tight_layout()

class PyShine_LIVE_PLOT_APP(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)        
        self.threadpool = QtCore.QThreadPool()	        
        self.canvas = MplCanvas(self, width=50.26, height=10.5, dpi=96, facecolor=(0, 0, 0, 0))
        self.canvas.setAutoFillBackground(False)  # Disabilita il riempimento automatico dello sfondo
        self.canvas.setStyleSheet("background-color: rgba(255, 0, 255, 0);")  # Imposta lo sfondo del canvas su completamente trasparente
        self.canvas.setWindowOpacity(0) 
 
     

        self.reference_plot = None
        self.q = queue.Queue(maxsize=20)
        self.go_on = ''
        self.device = 0
        self.window_length = 1000
        self.downsample = 3
        self.channels = [1]
        self.interval = 25 

        self.samplerate = 44100
        self.length  = int(self.window_length*self.samplerate/(1000*self.downsample))
        sd.default.samplerate = self.samplerate
        self.plotdata =  np.zeros((self.length,len(self.channels)))  
        self.update_plot()
        
    def getAudio(self):
        try:
            QtWidgets.QApplication.processEvents()	
            def audio_callback(indata,frames,time,status):
                self.q.put(indata[::self.downsample,[0]])
            
            stream  = sd.InputStream( device = self.device, channels = max(self.channels), samplerate =self.samplerate, callback  = audio_callback)
            with stream:
                while True:
                    QtWidgets.QApplication.processEvents()
                    if self.go_on:
                        break
        except Exception as e:
            print("ERROR: ",e)

    def start_worker(self):
        self.go_on = False
        worker = Worker(self.start_stream, )
        self.threadpool.start(worker)	     
 
    def start_stream(self): 
        self.getAudio()
   
    def update_plot(self):
        try:
            data=[0]
            
            while self.go_on is False:
                try: 
                    data = self.q.get_nowait()
                except queue.Empty:
   
                    break
                shift = len(data)
                self.plotdata = np.roll(self.plotdata, -shift,axis = 0)
                self.plotdata[-shift:,:] = data
                self.ydata = self.plotdata[:]
                self.canvas.axes.set_facecolor((0, 0, 0, 0))
                
                if self.reference_plot is None:
                    plot_refs = self.canvas.axes.plot( self.ydata, color=(0.20,0.60,1.00))
                    self.reference_plot = plot_refs[0]				
                else:
                    self.reference_plot.set_ydata(self.ydata)

            start, end = self.canvas.axes.get_ylim()
            self.canvas.axes.yaxis.set_ticks(np.arange(start, end, 0.1))
            self.canvas.axes.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
            self.canvas.axes.set_ylim( ymin=-0.3, ymax=0.3)	

            # Rimuovi le etichette e i titoli degli assi
            self.canvas.axes.set_xticklabels([])
            self.canvas.axes.set_yticklabels([])
            self.canvas.axes.set_xlabel('')
            self.canvas.axes.set_ylabel('')	

            # Rimuovi i bordi e la griglia
            self.canvas.axes.spines['top'].set_visible(False)
            self.canvas.axes.spines['right'].set_visible(False)
            self.canvas.axes.spines['left'].set_visible(False)
            self.canvas.axes.spines['bottom'].set_visible(False)
            self.canvas.axes.grid(False)

            # Rimuovi i trattini sugli assi
            self.canvas.axes.tick_params(axis='x', which='both', bottom=False, top=False)
            self.canvas.axes.tick_params(axis='y', which='both', left=False, right=False)
            self.canvas.draw()
        except Exception as e :
            print(e)

# www.pyshine.com
class Worker(QtCore.QRunnable):

    def __init__(self, function, *args, **kwargs):
        super(Worker, self).__init__()
        self.function = function
        self.args = args
        self.kwargs = kwargs        

    @Slot()
    def run(self):
        self.function(*self.args, **self.kwargs)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = PyShine_LIVE_PLOT_APP()
    mainWindow.show()
    sys.exit(app.exec_())