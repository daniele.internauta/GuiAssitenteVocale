from PySide2.QtCore import  QThreadPool, QSemaphore
from sintesiVocale.vocalist import ParlaWorker


def thread_parla(testo):
    # semafero = QSemaphore(0)
    parla = ParlaWorker(testo)
    QThreadPool.globalInstance().start(parla)
    # semafero.acquire()
    print(testo)

