import  time, json
from PySide2.QtCore import  QRunnable,QThreadPool, Signal,QObject,QSemaphore
from sintesiVocale.inizializza_sintesi_v import inzializza_sintesi_vocale
import sounddevice as sd

class ParlaWorker(QRunnable):
    segnale_fine = Signal()
    def __init__(self,testo):
        super().__init__()
         # Inizializza il motore di sintesi vocale
        self.engine2 = inzializza_sintesi_vocale()
        self.testo = testo

        # self.semafero = semafero
    def run(self):
        self.parla(self.testo)
        self.segnale_fine.emit()
        
        
    
        
        

def parla(testo):
    print("sono parla")
    
    with open("data.json", "r") as f:
        volume_voce_dict  = json.load(f)
    engine2 = inzializza_sintesi_vocale()
    engine2.setProperty('rate', volume_voce_dict["velocita"])
    
    engine2.setProperty('voice', volume_voce_dict["voce"])
    # engine2.setProperty('voice', volume_voce_dict["voce"])
    engine2.setProperty('volume', volume_voce_dict["volume"])

    
    # Fai leggere il testo dalla voce selezionata
    engine2.say(testo)
    # Aspetta che il testo sia stato completamente letto

    engine2.runAndWait()
    # self.signal.emit("fine") 
   
        
      
           
if __name__ == "__main__":
        
        parla = ParlaWorker(QRunnable)
        audio_data = parla.parla("Benvenuto nel programma di assistenza vocale!")
       