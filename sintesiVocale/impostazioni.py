import json, os
from sintesiVocale.inizializza_sintesi_v import inzializza_sintesi_vocale

class impostazioni_audio():
    def __init__(self):
        # Inizializza il motore di sintesi vocale
        engine1 = inzializza_sintesi_vocale()
        self.dict_impostazioni = {}
        # Ottenere l'elenco di tutte le voci disponibili nel sistema
        self.voci = engine1.getProperty('voices') 

        # Ottengo la chiave della voce attualmente in uso
        self.chiave_voce_attuale = engine1.getProperty('voice')
        self.dict_impostazioni["voce"] = self.chiave_voce_attuale
         # Ottieni i nomi delle voci disponibili
        nomi_voci = list([index, voce.name] for index, voce in enumerate(self.voci) if voce.id == self.chiave_voce_attuale)
     
        self.dict_impostazioni["index"] = nomi_voci[0][0]
        self.dict_impostazioni["nome"] = nomi_voci[0][1]
        #C ottengo il volume e la velocità impostate nel sistema
        self.volume = engine1.getProperty('volume')
        self.dict_impostazioni["volume"] = self.volume
        self.velocita = engine1.getProperty('rate')
        self.dict_impostazioni["velocita"] = self.velocita
        engine1.stop()
    
       


    def scrivi_impostazioni(self):
        if not os.path.exists("data.json"):
            with open("data.json", "w") as json_file:
                json.dump(self.dict_impostazioni, json_file)
        

    def nome_voce(self):
        # Trovare il nome della voce corrispondente all'ID ottenuto
        nome_voce_attuale = next((voce.name for voce in self.voci if voce.id == self.chiave_voce_attuale), None)
        return nome_voce_attuale
    
    def lista_voci(self):
        #recupro in un dizionario il nome e l'id di ogni voce del sistema
        dict_voci ={}
        for indx, voce in enumerate(self.voci):
            if "Italian" in voce.name:
                dict_voci[voce.name] = voce.id
        return(dict_voci)

