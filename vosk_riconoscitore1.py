import os
import re
import json
import time
import subprocess
from vosk import Model, KaldiRecognizer, SetLogLevel
import pyaudio
from PySide2.QtCore import QRunnable, Signal, QObject, Slot
from sintesiVocale.vocalist import parla

class AscoltaWorker(QRunnable, QObject):
    # Definisci segnali personalizzati per la gestione del testo riconosciuto e della spesa scritta
    testoRiconoscimeto = Signal(str)
    spesaScritta = Signal(str)

    def __init__(self, ric):
        super(AscoltaWorker, self).__init__()
        self.riconoscimento = ric

    def run(self):
        # Connetti i segnali ai loro slot corrispondenti
        self.riconoscimento.segnale.textRiconosciuto.connect(self.inTestoRiconosciuto)
        self.riconoscimento.segnale.txtspesaScritta.connect(self.inSpesaScritta)
        self.riconoscimento.ascolto()

    @Slot(str)
    def inTestoRiconosciuto(self, testo):
        # Emetti il segnale con il testo riconosciuto
        self.testoRiconoscimeto.emit(testo)

    @Slot(list)
    def inSpesaScritta(self, elementi_s):
        # Emetti il segnale con la lista della spesa scritta
        self.spesaScritta.emit(str(elementi_s))

    def interrompere(self):
        # Interrompi l'ascolto
        self.riconoscimento.spegni_tutto()

class RiconoscimentoVosk(QRunnable):
    class Segnali(QObject):
        # Definisci segnali per il testo riconosciuto e la lista della spesa
        textRiconosciuto = Signal(str)
        txtspesaScritta = Signal(list)

    def __init__(self):
        super(RiconoscimentoVosk, self).__init__()
        self.frequenzaHz = 16000
        self.buffer_frame = 1280
        self.nome_file_spesa = "ListaSpesa.txt"
        self.segnale = RiconoscimentoVosk.Segnali()
        self.terminazione_richiesta = False
        self.parola_chiave_attivazione = False

        # Imposta il livello di log e inizializza il modello Vosk
        SetLogLevel(-1)
        modello = Model(r'F:\PytonProject\vosk-model-it-0.22')
        self.recognizer = KaldiRecognizer(modello, self.frequenzaHz)
        parla("Benvenuto!")

    def spegni_tutto(self):
        # Ferma lo streaming e segnala la terminazione
        self.stream.stop_stream()
        self.terminazione_richiesta = True

    def avvio_streaming(self):
        # Avvia lo streaming audio
        self.cattura = pyaudio.PyAudio()
        self.stream = self.cattura.open(format=pyaudio.paInt16, channels=1,
                                        rate=self.frequenzaHz,
                                        frames_per_buffer=self.buffer_frame,
                                        input=True)
        if not self.stream.is_active():
            self.stream.start_stream()

    def stop_streaming(self):
        # Termina lo streaming audio
        if self.stream.is_active():
            self.cattura.terminate()
            self.stream.stop_stream()

    def leggi_e_riconosci_audio(self):
        # Leggi e riconosci l'audio dal microfono
        self.avvio_streaming()
        data = self.stream.read(self.buffer_frame, exception_on_overflow=False)
        if self.recognizer.AcceptWaveform(data):
            risultato_json_string = self.recognizer.Result()
            risultato = json.loads(risultato_json_string)
            return risultato['text']
        else:
            return None

    # Metodo per gestire il comando di attivazione
    def gestisci_comondo_chiamata(self, testo):
        if "ivona" in testo:
            self.parola_chiave_attivazione = True

    # Metodo per gestire il comando di stampa della lista della spesa
    def gestisci_comando_stampa_lista_spesa(self, testo):
        if "stampa spesa" in testo:
            self.stop_streaming()
            self.stampa_cancella()
            self.parola_chiave_attivazione = False

    # Metodo per gestire il comando di scrittura della lista della spesa
    def gestisci_comando_scrivi_spesa(self, testo):
        spesa_scrivi = re.compile(r'\bscrivi\b.*\bspesa\b')
        spesa_scrivi_match = re.search(spesa_scrivi, testo)
        if spesa_scrivi_match:
            self.stop_streaming()
            self.scrivi_lista_spesa()
            self.parola_chiave_attivazione = False

    # Metodo principale per l'ascolto continuo
    def ascolto(self):
        while not self.terminazione_richiesta:
            testo_riconosciuto = self.leggi_e_riconosci_audio()
            if testo_riconosciuto:
                self.gestisci_comondo_chiamata(testo_riconosciuto)
                if self.parola_chiave_attivazione:
                    self.segnale.textRiconosciuto.emit(testo_riconosciuto)
                    self.gestisci_comando_scrivi_spesa(testo_riconosciuto)
                    self.gestisci_comando_stampa_lista_spesa(testo_riconosciuto)
                    print(testo_riconosciuto)

    # Metodo per scrivere la lista della spesa su file
    def scrivi_file_spesa(self, testo):
        elementi_spesa = testo.split()

        with open(self.nome_file_spesa, "a+", encoding='utf-8') as file:
            for elemento_s in elementi_spesa:
                elemento_f = "- " + elemento_s + '-\n'
                file.write(elemento_f)
                file.seek(0)
                elementi_scritti = [riga.strip() for riga in file.readlines()]
        return elementi_scritti

    # Metodo per scrivere la lista della spesa tramite riconoscimento vocale
    def scrivi_lista_spesa(self):
        parla("Detta la lista della spesa!")
        self.avvio_streaming()
        start_time = time.time()
        durata = 30
        while time.time() - start_time < durata:
            testo_riconosciuto = self.leggi_e_riconosci_audio()
            if testo_riconosciuto:
                if "fine" in testo_riconosciuto:
                    break
                print(testo_riconosciuto)
                elementi_scritti = self.scrivi_file_spesa(testo_riconosciuto)
        try:
            self.segnale.txtspesaScritta.emit(elementi_scritti)
        except Exception as e:
            print(e)
        self.stop_streaming()

    # Metodo per stampare e cancellare la lista della spesa
    def stampa_cancella(self):
        if self.file_esiste_non_vuoto():
            subprocess.run(['notepad.exe', '/p', self.nome_file_spesa])
            with open(self.nome_file_spesa, "w") as f:
                pass
        else:
            parla("La lista non è stata scritta")

    # Metodo per verificare se il file della lista della spesa esiste e non è vuoto
    def file_esiste_non_vuoto(self):
        if not os.path.exists(self.nome_file_spesa):
            return False
        with open(self.nome_file_spesa, "r") as f:
            if f.read() == "":
                return False
        return True

if __name__ == "__main__":
    riconoscimento = RiconoscimentoVosk()
    riconoscimento.ascolto()
