from PySide2 import  QtCore
from PySide2.QtCore import QRect, QPropertyAnimation, QPoint



def anima_lancette(self, direzione_vento):
    # Crea l'animazione
    self.animazione_rotazione = QPropertyAnimation(self,  b'direzione_lancetta')
    self.animazione_rotazione.setDuration(2000)  # Durata dell'animazione in millisecondi
    self.animazione_rotazione.setStartValue(self.direzione_lancetta)  # Valore iniziale della rotazione
    self.animazione_rotazione.setEndValue(direzione_vento)  # Valore finale della rotazione
    self.animazione_rotazione.start()  # Avvia l'animazione
    
def animazioneFrameMeteo(self, frame):
    # Memorizzo la dimensione del frame
    posY = frame.y()
    posX = frame.x()

    larghezza = frame.width()
    altezza =  frame.height()

    if altezza == 149:
        altezza_m = 208
    else:
        altezza_m = 149

    # verifico le dimensioni se sono uguali a zero
    if posY == 46:
        posizioneY = 185
    else:
        posizioneY = 46
   
    # inzializzo l'oggetto da animare specificando il parametro 
    self.animazione_altezza = QPropertyAnimation(frame, b"geometry")
    # imposto una durata all'animazione
    self.animazione_altezza .setDuration(700)
    # imposto i valori di start dell'animazione passando alla geometria la larghezza iniziale   
    self.animazione_altezza .setStartValue(QRect(posX, posY,larghezza, altezza))
    # imposto i valori di fine animazione passando la largezza finale 
    self.animazione_altezza .setEndValue(QRect(posX,posizioneY,larghezza, altezza_m))
    # print(dir(QtCore.QEasingCurve)) # stampa tutte le funzioni possibili sulle animazioni 
    self.animazione_altezza .setEasingCurve(QtCore.QEasingCurve.InOutExpo)
    # self.animazione_altezza .setLoopCount(-1)
    self.animazione_altezza .start()
def animazioneFrameImpostazioni(self, frame):
    # Memorizzo la dimensione del frame
    posY = frame.y()
    posX = frame.x()

    larghezza = frame.width()
    altezza =  frame.height()

    if altezza == 158:
        altezza_m = 189
    else:
        altezza_m = 158

    # verifico le dimensioni se sono uguali a zero
    if posY == 39:
        posizioneY = 177
    else:
        posizioneY = 39
   
    # inzializzo l'oggetto da animare specificando il parametro 
    self.animazione_altezza = QPropertyAnimation(frame, b"geometry")
    # imposto una durata all'animazione
    self.animazione_altezza .setDuration(700)
    # imposto i valori di start dell'animazione passando alla geometria la larghezza iniziale   
    self.animazione_altezza .setStartValue(QRect(posX, posY,larghezza, altezza))
    # imposto i valori di fine animazione passando la largezza finale 
    self.animazione_altezza .setEndValue(QRect(posX,posizioneY,larghezza, altezza_m))
    # print(dir(QtCore.QEasingCurve)) # stampa tutte le funzioni possibili sulle animazioni 
    self.animazione_altezza .setEasingCurve(QtCore.QEasingCurve.InOutExpo)
    # self.animazione_altezza .setLoopCount(-1)
    self.animazione_altezza .start()

    # # inzializzo l'oggetto da animare specificando il parametro 
    # self.animation = QPropertyAnimation(frame, b"pos")
    # # imposto una durata all'animazione
    # self.animation.setDuration(700)
    # # imposto i valori di start dell'animazione passando alla geometria la larghezza iniziale   
    # self.animation.setStartValue(QPoint(posX, posY))
    # # imposto i valori di fine animazione passando la largezza finale 
    # self.animation.setEndValue(QPoint(posX,posizioneY))
    # # print(dir(QtCore.QEasingCurve)) # stampa tutte le funzioni possibili sulle animazioni 
    # self.animation.setEasingCurve(QtCore.QEasingCurve.InOutExpo)
    # # self.animation.setLoopCount(-1)
    # self.animation.start()

def animazioneFrameAscolto(self, frame):
    # Memorizzo la dimensione del frame
    posY = frame.pos().y()
    posX = frame.pos().x()
    
    # verifico le dimensioni se sono uguali a zero
    if posY == 58:
        posizioneY = 3
    else:
        posizioneY = 58

    # inzializzo l'oggetto da animare specificando il parametro 
    self.animation = QPropertyAnimation(frame, b"pos")
    # imposto una durata all'animazione
    self.animation.setDuration(700)
    # imposto i valori di start dell'animazione passando alla geometria la larghezza iniziale   
    self.animation.setStartValue(QPoint(posX, posY))
    # imposto i valori di fine animazione passando la largezza finale 
    self.animation.setEndValue(QPoint(posX,posizioneY))
    # print(dir(QtCore.QEasingCurve)) # stampa tutte le funzioni possibili sulle animazioni 
    self.animation.setEasingCurve(QtCore.QEasingCurve.InOutExpo)
    # self.animation.setLoopCount(-1)
    self.animation.start()