# Importazione dei moduli necessari da PySide2
from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtCore import QPoint, QTimer, QThreadPool,Property, Qt ,QRect
from PySide2.QtGui import QPixmap
from PySide2.QtWidgets import QApplication, QMainWindow

# Importazione di altri moduli richiesti
import json, time
import sys
from PIL import Image
from PIL import ImageQt
# Importazione dei moduli personalizzati
from main_ui import Ui_MainWindow
from graficoAudio import PyShine_LIVE_PLOT_APP
from vosk_riconoscitore import AscoltaWorker, RiconoscimentoVosk
import orologio
from anima_finestra import animazioneFrameImpostazioni, animazioneFrameAscolto, animazioneFrameMeteo, anima_lancette
from sintesiVocale.impostazioni import impostazioni_audio
from meteo import Meteo


class MainWindow(QMainWindow):
    """
    Classe principale che rappresenta la finestra dell'applicazione.
    """

    def __init__(self):
        """
        Costruttore della classe MainWindow.
        """
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Impostazioni della finestra per renderla senza bordi e trasparente
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        # Configurazione degli eventi del mouse per il contenitore
        self.ui.sfondo.mousePressEvent = self.on_mouse_press
        self.ui.sfondo.mouseMoveEvent = self.on_mouse_move
        self.ui.sfondo.mouseReleaseEvent = self.on_mouse_release

        # Creazione e configurazione del visualizzatore audio
        self.audio_visualizer = PyShine_LIVE_PLOT_APP()
        self.ui.gridLayout.addWidget(self.audio_visualizer.canvas, 0, 0, 1, 1)
        self.audio_visualizer.canvas.hide()

        # Configurazione del timer per l'aggiornamento del grafico audio
        self.timer = QTimer()
        self.timer.setInterval(self.audio_visualizer.interval)  # msec
        self.timer.timeout.connect(self.audio_visualizer.update_plot)

        # Configurazione del timer per l'aggiornamento dell'orologio
        self.aggiornamento_ora = orologio.AggiornamentoOra()
        self.aggiornamento_ora.timer.start()

        # Timer per l'aggiornamento del label dell'orologio
        self.timer_label = QTimer()
        self.timer_label.setInterval(100)  # Aggiorna ogni 100 millisecondi
        self.timer_label.timeout.connect(self.aggiorna_label)

        # Nascondere gli elementi grafici "Neon"
        self.ui.NeonSotto.hide()
        self.ui.NeonSopra.hide()

        # Inizializzazione della variabile di ascolto
        self.ascolta = None

        # Timer per l'intervallo di tempo tra le pressioni del pulsante generale
        self.time_pulsante = QTimer()
        self.time_pulsante.setInterval(3000)
        self.time_pulsante.timeout.connect(self.abilita_pulsante)

        # Timer per intercettare la pressione del pulsante delle impostazioni
        self.time_bottone_impostazioni = QTimer()
        self.time_bottone_impostazioni.setInterval(1000)
        self.time_bottone_impostazioni.timeout.connect(self.abilita_pulsante_impostazioni)

        # Timer per i dati metereologici 
        self.timer_meteo = QTimer()
        self.timer_meteo.setInterval(900000)# 15minuti
        self.timer_meteo.timeout.connect(self.aggiorna_dati_meteo)
        self.dati_meteo = Meteo()
        
        # Caricamento delle impostazioni audio e aggiornamento della combobox
        self.imp_audio = impostazioni_audio()
        self.id_voci = list(self.imp_audio.lista_voci().values())
        self.nomi_voci = list(self.imp_audio.lista_voci().keys())
        self.imp_audio.scrivi_impostazioni()

        # Lettura del file JSON delle impostazioni
        with open("data.json", "r") as json_file:
            self.dict_impostazioni = json.load(json_file)

        # Aggiornamento della combobox con le voci disponibili
        self.ui.comboBox.addItems(self.nomi_voci)
        self.ui.comboBox.currentIndexChanged.connect(self.update_voci)
        self.ui.comboBox.setCurrentIndex(self.dict_impostazioni['index'])

        # Impostazione del volume e della velocità di lettura
        self.impostazione_volume = self.dict_impostazioni["volume"] * 100
        self.ui.Volume.setValue(self.impostazione_volume)
        self.ui.spinBoxRate.setValue(self.dict_impostazioni["velocita"])
        self.ui.spinBoxRate.valueChanged.connect(self.update_rate)
        self.ui.Volume.valueChanged.connect(self.dial_volume)
        self.ui.lcdNumber.display("")

        self.ui.OnOff_meteo.clicked.connect(self.meteo_on_off)

    # Definizione dei metodi per la gestione degli eventi del mouse
    def on_mouse_press(self, event):
        """
        Gestisce l'evento di pressione del mouse sul label.
        """
        if event.button() == QtCore.Qt.LeftButton:
            self.offset = QPoint(event.pos().x(), event.pos().y())
        else:
            super().mousePressEvent(event)

    def on_mouse_move(self, event):
        """
        Gestisce l'evento di movimento del mouse sul label.
        """
        if self.offset is not None and event.buttons() == QtCore.Qt.LeftButton:
            self.move(self.pos() + event.pos() - self.offset)
        else:
            super().mouseMoveEvent(event)

    def on_mouse_release(self, event):
        """
        Gestisce l'evento di rilascio del mouse sul label.
        """
        self.offset = None
        super().mouseReleaseEvent(event)

    # Metodi aggiuntivi della classe MainWindow
    def abilita_pulsante(self):
        """
        Abilita il pulsante generale dopo un intervallo di tempo.
        """
        self.ui.OnOff_Generale.setEnabled(True)
        self.time_pulsante.stop()

    def abilita_pulsante_impostazioni(self):
        """
        Abilita il pulsante delle impostazioni dopo un intervallo di tempo.
        """
        self.ui.OnOff_Impostazioni.setEnabled(True)
        self.time_bottone_impostazioni.stop()

    def aggiorna_testo_parlato(self, testo):
        """
        Aggiorna il testo visualizzato con il testo parlato riconosciuto.
        """
        self.ui.text_ascolto.setText(testo)

    def aggiorna_label(self):
        """
        Aggiorna il label dell'orologio con l'ora corrente formattata.
        """
        self.ui.orologio.setText(self.aggiornamento_ora.get_ora_formattata())

    def aggiorna_dati_meteo(self):
        info_meteo = self.dati_meteo.dizio_info_base
        cordinate = info_meteo["cordinate"]
        print(f'cordinate long lat: {cordinate}')
        altitudine = info_meteo['altitudine']
        print(f'ALTITUDINE: {altitudine}')
        info_meteo_correnti = self.dati_meteo.info_correnti()
        direzione_vento = info_meteo_correnti['Direzione Vento']
        velocita_vento = info_meteo_correnti['Velocita Vento']
        temperatura = info_meteo_correnti['Temperatura']
        self.aggiorna_img_temperatura(temperatura)
        velocita_vento_m = round(velocita_vento, 2)
        self.ui.VelocitaVento.setText(str(velocita_vento_m))
        self.ui.temperatura.setText(str(temperatura))
        # Timer per i dati metereologici 
        self.timer_lancetta = QTimer()
        self.timer_lancetta.setInterval(50)# 15minuti
        self.timer_lancetta.timeout.connect(lambda:self.aggiorna_lancetta_vento(direzione_vento))
        self.timer_lancetta.start()
        self.rotazione = 0
        self.stop_rotazione = 0
        self.avanzamento = 360/60
        self.lancetta = Image.open("img/Meteo/LancettaRosaDeiVenti.png")
          
    def aggiorna_img_temperatura(self,temperatura):
        if temperatura > 15:
            self.ui.temperatura_img.setPixmap(QPixmap(u":/meteo/img/Meteo/TermometroCaldo.png"))
        else:
            self.ui.temperatura_img.setPixmap(QPixmap(u":/meteo/img/Meteo/TermometroFreddo.png"))

 
    def aggiorna_lancetta_vento(self, dir_vento):
            if self.rotazione < dir_vento:
                self.rotazione = self.rotazione + self.avanzamento
            else:
                self.timer_lancetta.stop()
            
            rotazione_lancetta = QPixmap.fromImage(ImageQt.ImageQt(self.lancetta.rotate(-int(self.rotazione))))
            self.ui.RosaDeiVenti_Lancetta.setPixmap(rotazione_lancetta)

    def aggiorna_impostazioni(self, data):
        """
        Aggiorna il file JSON delle impostazioni con i nuovi dati.
        """
        with open("data.json", "w") as json_file:
            json.dump(data, json_file)

    def update_rate(self, value):
        """
        Aggiorna la velocità di lettura nel dizionario delle impostazioni.
        """
        self.dict_impostazioni["velocita"] = value
        self.aggiorna_impostazioni(self.dict_impostazioni)


    def update_voci(self, index):
        """
        Aggiorna la voce selezionata nel dizionario delle impostazioni.
        """
        self.dict_impostazioni["voce"] = self.id_voci[index]
        self.dict_impostazioni["nome"] = self.nomi_voci[index]
        self.dict_impostazioni["index"] = index
        lista_voci = {"Carla": "Ivona Q.I.A", "Giorgio": "Ivone Q.I.A", "Elsa": "Evona Q.I.A"}
        for l_v in lista_voci:
            if l_v in self.dict_impostazioni["nome"]:
                self.ui.titolo.setText(lista_voci[l_v])

        self.aggiorna_impostazioni(self.dict_impostazioni)

    def dial_volume(self):
        """
        Aggiorna il volume nel dizionario delle impostazioni e visualizza il valore sul display.
        """
        value = self.ui.Volume.value()
        self.ui.lcdNumber.setProperty("value", str(value))
        self.dict_impostazioni["volume"] = value / 100
        self.aggiorna_impostazioni(self.dict_impostazioni)


    # Metodi per la gestione delle impostazioni e dell'interfaccia utente

    def on_off_impostazioni(self, on_off):
        """
        Gestisce l'attivazione o la disattivazione delle impostazioni.
        """
        if on_off:
            self.ui.NeonSotto.show()
            self.ui.OnOff_Impostazioni.setEnabled(False)
            animazioneFrameImpostazioni(self, self.ui.frameImpostazioni)
            self.time_bottone_impostazioni.start()
            print("sono Acceso")
        else:
            self.ui.NeonSotto.hide()
            self.ui.OnOff_Impostazioni.setEnabled(False)
            animazioneFrameImpostazioni(self, self.ui.frameImpostazioni)
            print("sono Spento")
            self.time_bottone_impostazioni.start()

    def meteo_on_off(self, on_off):
        if on_off:
            self.timer_meteo.start()
            self.aggiorna_dati_meteo()
            animazioneFrameMeteo(self,self.ui.frameMeteo)
            print(on_off)
        else:
            self.timer_meteo.stop()
            animazioneFrameMeteo(self,self.ui.frameMeteo)
            print(on_off)

    def chiudi_on_off(self, on_off):
        """
        Gestisce la chiusura dell'applicazione.
        """
        self.on_off_generale(False)
        self.close()

    def on_off_generale(self, on_off):
        """
        Gestisce l'attivazione o la disattivazione generale dell'applicazione.
        """
        if on_off:
            self.ui.OnOff_meteo.setEnabled(True)
            self.ui.Volume.setEnabled(True)
            self.ui.lcdNumber.setProperty("value", self.impostazione_volume)
            self.ui.OnOff_Generale.setEnabled(False)
            self.time_pulsante.start()
            animazioneFrameAscolto(self, self.ui.frameAscolto)
            self.ui.NeonSopra.show()
            

            # Avvio il timer, faccio visualizzare il canvas, avvio il thread e aggiorno il plot
            self.timer.start()
            self.audio_visualizer.canvas.show()
            self.audio_visualizer.start_worker()
            self.audio_visualizer.update_plot()

            # Modifico gli stili dei label on off
            self.ui.OnLabel.setStyleSheet(u"color: rgb(0, 141, 211);\n"
                                          "font-size:18px;\n"
                                          "text-align:center;")
            self.ui.OffLabel.setStyleSheet(u"color: rgb(118, 0, 0);\n"
                                           "font-size:14px;\n"
                                           "text-align:center;")

            # Avvio il loop per aggiornare l'ora sul label
            self.timer_label.start()
            self.ui.sfondo.setPixmap(QPixmap(u":/sfondo/img/contenitoreHOWER.png"))

            # Istanziamento di RiconoscimentoVosk e AscoltaWorker che lancio nel threadpool e connetto il segnale del metodo
            self.riconoscitore = RiconoscimentoVosk()
            self.ascolta = AscoltaWorker(self.riconoscitore)
            self.ascolta.testoRiconoscimeto.connect(self.aggiorna_testo_parlato)
            self.ascolta.spesaScritta.connect(self.aggiorna_testo_parlato)
            QThreadPool.globalInstance().start(self.ascolta)

            self.ui.TarghettaImpostazioniOFF.setEnabled(True)
            self.ui.OnOff_Impostazioni.setEnabled(True)
        else:
            self.ui.OnOff_meteo.setEnabled(False)

            if self.ui.OnOff_Impostazioni.isChecked():
                self.on_off_impostazioni(False)
                self.ui.OnOff_Impostazioni.setChecked(False)
            self.ui.Volume.setEnabled(False)
            self.ui.lcdNumber.display("")
            self.ui.OnOff_Generale.setEnabled(False)
            self.time_pulsante.start()
            self.ui.OnOff_Impostazioni.setEnabled(False)
            self.ui.TarghettaImpostazioniOFF.setEnabled(False)
            try:
                if self.ascolta:
                    self.ui.NeonSopra.hide()
                    # Interrompo i cicli del thread e svuoto la memoria cancellando le istanze
                    self.ascolta.interrompere()
                    del self.riconoscitore
                    del self.ascolta
                    # Fermo tutte le funzioni che generano il grafico
                    self.timer.stop()
                    self.audio_visualizer.go_on = True
                    self.audio_visualizer.q.queue.clear()
                    self.audio_visualizer.canvas.hide()
                    animazioneFrameAscolto(self, self.ui.frameAscolto)
                    # Modifico gli stili dei label on off
                    self.ui.OnLabel.setStyleSheet(u"color: rgb(0, 83, 125);\n"
                                                  "font-size:18px;\n"
                                                  "text-align:center;")
                    self.ui.OffLabel.setStyleSheet(u"color: rgb(255, 0, 0);\n"
                                                   "font-size:14px;\n"
                                                   "text-align:center;")
            except AttributeError as a:
                print("ascolta è stato eliminata come variabile", a)
            # Imposto l'orologio su off togliendo il testo e stoppando il timerLoop
            self.timer_label.stop()
            self.ui.orologio.setText("")
            # Imposto lo sfondo del main principale
            self.ui.sfondo.setPixmap(QPixmap(u":/sfondo/img/contenitore.png"))

# Il punto di ingresso principale dell'applicazione
if __name__ == "__main__":
    app = QApplication([])
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

