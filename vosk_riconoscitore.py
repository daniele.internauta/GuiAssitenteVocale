from vosk import Model, KaldiRecognizer, SetLogLevel
import pyaudio, time, json, re, subprocess, os
from PySide2.QtCore import  QRunnable,Signal,QObject,QThreadPool,Slot
from sintesiVocale.vocalist import parla

class AscoltaWorker(QRunnable,QObject):
   # Definisci un segnale personalizzato
    testoRiconoscimeto = Signal(str)
    spesaScritta = Signal(str)

    def __init__(self, ric):
        QRunnable.__init__(self)
        QObject.__init__(self)
        self.riconoscimento = ric
        
        
    def run(self):
        # Connetti i segnali ai loro slot
        self.riconoscimento.segnale.textRiconosciuto.connect(self.inTestoRiconosciuto)
        self.riconoscimento.segnale.txtspesaScritta.connect(self.inSpesaScritta)
        self.riconoscimento.ascolto()   

    @Slot(str)
    def inTestoRiconosciuto(self, testo):
        # Gestisci qui il testo riconosciuto
        self.testoRiconoscimeto.emit(testo)

    @Slot(list)
    def inSpesaScritta(self, elementi_s):
        # Gestisci qui la lista della spesa scritta
        self.spesaScritta.emit(str(elementi_s))

    def interrompere(self):
        self.riconoscimento.spegni_tutto()

class RiconoscimentoVosk(QRunnable):
    class Segnali(QObject):
        textRiconosciuto = Signal(str)
        txtspesaScritta = Signal(list)
    def __init__(self):
        super().__init__()
        self.frequenzaHz = 16000
        self.buffer_frame = 1280
        self.nome_file_spesa = "ListaSpesa.txt"
        self.segnale = RiconoscimentoVosk.Segnali()
        
        SetLogLevel(-1)
        modello = Model(r'F:\PytonProject\vosk-model-it-0.22')
        
        self.recognizer = KaldiRecognizer(modello, self.frequenzaHz)
        self.terminazione_richiesta = False
        self.parola_chiave_attivazione = False
        parla("Benvenuto!")
       

    def spegni_tutto(self):
        self.stream.stop_stream()
        self.terminazione_richiesta = True

    def avvio_streaming(self):
        self.cattura = pyaudio.PyAudio()
        self.stream = self.cattura.open(format=pyaudio.paInt16, channels=1, 
                                rate=self.frequenzaHz,
                                frames_per_buffer=self.buffer_frame,  
                                input=True
                                )
        if not self.stream.is_active():
            self.stream.start_stream() 
        
    def stop_streaming(self):
        if self.stream.is_active():
            self.cattura.terminate()
            self.stream.stop_stream()

    def leggi_e_riconosci_audio(self):
        self.avvio_streaming()
        data = self.stream.read(self.buffer_frame, exception_on_overflow=False)
        if self.recognizer.AcceptWaveform(data):
            risultato_json_string = self.recognizer.Result()
            risultato = json.loads(risultato_json_string)
            risposta = risultato['text']
            return risposta
        else:
            return None
        
    def gestisci_comondo_chiamata(self,testo):
        if "ivona" in testo:
            self.parola_chiave_attivazione = True

    def gestisci_comando_stampa_lista_spesa(self,testo):
        if "stampa spesa" in testo:
            self.stop_streaming()
            self.stampa_cancella()
            self.parola_chiave_attivazione = False


    def gestisci_comando_scrivi_spesa(self,testo):
        print("riconosciutio")
        spesa_scrivi = re.compile(r'\bscrivi\b.*\bspesa\b')
        spesa_scrivi_match = re.search(spesa_scrivi , testo)
        if spesa_scrivi_match:
            self.stop_streaming()
            self.scrivi_lista_spesa()
            self.parola_chiave_attivazione = False


    def ascolto(self):
        while not self.terminazione_richiesta:
            testo_riconosciuto = self.leggi_e_riconosci_audio()
            if testo_riconosciuto:
                self.gestisci_comondo_chiamata(testo_riconosciuto)
                if self.parola_chiave_attivazione:
                    self.segnale.textRiconosciuto.emit(testo_riconosciuto)
                    self.gestisci_comando_scrivi_spesa(testo_riconosciuto)
                    self.gestisci_comando_stampa_lista_spesa(testo_riconosciuto)
                    print(testo_riconosciuto)
 
    def scrivi_file_spesa(self,testo):
        elementi_spesa = testo.split()
                
        with open(self.nome_file_spesa, "a+", encoding='utf-8') as file: 
            for elemnto_s in elementi_spesa:
                elemento_f = "- " + elemnto_s + '-\n'
                file.write(elemento_f)
                file.seek(0)
                elementi_scritti = [riga.strip() for riga in file.readlines()]
        return elementi_scritti 
                                           
    def scrivi_lista_spesa(self):
        parla("Detta la lista della spesa!")
        self.avvio_streaming()
        start_time = time.time()
        durata = 30
        # elementi_scritti = ''
        while time.time() - start_time < durata:
            testo_riconosciuto = self.leggi_e_riconosci_audio()
            if testo_riconosciuto:
                if "fine" in testo_riconosciuto:
                    break 
                print(testo_riconosciuto) 
                elementi_scritti = self.scrivi_file_spesa(testo_riconosciuto)     
        try:
            self.segnale.txtspesaScritta.emit(elementi_scritti)
            parla(f"Nella lista sono stati inseriti: {elementi_scritti}")
        except Exception as e:
            parla("Non è stato scritto nulla nella lista") 
        self.stop_streaming()
    
    def stampa_cancella(self):
        if self.file_esiste_non_vuoto():
            subprocess.run(['notepad.exe', '/p', self.nome_file_spesa])
            with open(self.nome_file_spesa, "w") as f:  
                pass 
        else:
            parla("La lista non e' stata scritta")
    
    def file_esiste_non_vuoto(self): 
        if not os.path.exists(self.nome_file_spesa): 
            return False 
        with open(self.nome_file_spesa, "r") as f: 
            if f.read() == "": 
                return False 
        return True 
 
if __name__ == "__main__":
    riconoscimento = RiconoscimentoVosk()
    riconoscimento.ascolto()