import openmeteo_requests

import requests_cache
import pandas as pd
from retry_requests import retry
from datetime import datetime, timedelta

class Meteo:
	def __init__(self) -> None:
		
		# Setup the Open-Meteo API client with cache and retry on error
		cache_session = requests_cache.CachedSession('.cache', expire_after = 3600)
		retry_session = retry(cache_session, retries = 5, backoff_factor = 0.2)
		openmeteo = openmeteo_requests.Client(session = retry_session)

		# Make sure all required weather variables are listed here
		# The order of variables in hourly or daily is important to assign them correctly below
		url = "https://api.open-meteo.com/v1/forecast"
		params = {
					"latitude": 45.126,
					"longitude": 7.6314,
					"current": ["temperature_2m", "is_day", "precipitation", "cloud_cover", "wind_speed_10m", "wind_direction_10m"],
					"daily": ["temperature_2m_max", "temperature_2m_min", "precipitation_sum", "precipitation_hours", "precipitation_probability_max", "wind_speed_10m_max", "wind_direction_10m_dominant"],
					"timezone": "Europe/Berlin",
					"forecast_days": 16
				}
		responses = openmeteo.weather_api(url, params=params)

		# Process first location. Add a for-loop for multiple locations or weather models
		self.response = responses[0]
		print(f"Coordinates {self.response.Latitude()}°N {self.response.Longitude()}°E")
		print(f"Elevation {self.response.Elevation()} m asl")
		print(f"Timezone {self.response.Timezone()} {self.response.TimezoneAbbreviation()}")
		print(f"Timezone difference to GMT+0 {self.response.UtcOffsetSeconds()} s")

		self.dizio_info_base = {}
		self.dizio_info_base["cordinate"] =  [self.response.Latitude(),self.response.Longitude()]
		self.dizio_info_base['altitudine'] = self.response.Elevation()

	def info_correnti(self):
		dizio_info_correnti = {}
		# Current values. The order of variables needs to be the same as requested.
		# Current values. The order of variables needs to be the same as requested.
		current = self.response.Current()
		
		current_temperature_2m = current.Variables(0).Value()
		current_is_day = current.Variables(1).Value()
		current_precipitation = current.Variables(2).Value()
		current_cloud_cover = current.Variables(3).Value()
		current_wind_speed_10m = current.Variables(4).Value()
		current_wind_direction_10m = current.Variables(5).Value()
		dizio_info_correnti['Temperatura'] = current_temperature_2m
		dizio_info_correnti['GiornoNotte'] = current_is_day
		dizio_info_correnti['Pioggia'] = current_precipitation
		dizio_info_correnti['Nuvoloso'] = current_cloud_cover
		dizio_info_correnti['Velocita Vento'] = current_wind_speed_10m
		dizio_info_correnti['Direzione Vento'] = current_wind_direction_10m
		print(f"Current time {current.Time()}")
		print(f"Current temperature_2m {current_temperature_2m}")
		print(f"Current is_day {current_is_day}")
		print(f"Current precipitation {current_precipitation}")
		print(f"Current cloud_cover {current_cloud_cover}")
		print(f"Current wind_speed_10m {current_wind_speed_10m}")
		print(f"Current wind_direction_10m {current_wind_direction_10m}")
		print("Sono il dizionario :", dizio_info_correnti)
		return dizio_info_correnti
		
	def info_previsioni(self,data_richiesta):
		# Converti la stringa in un oggetto datetime
		data_richiesta = pd.to_datetime(data_richiesta)
		# Process daily data. The order of variables needs to be the same as requested.
		daily = self.response.Daily()
		daily_temperature_2m_max = daily.Variables(0).ValuesAsNumpy()
		daily_temperature_2m_min = daily.Variables(1).ValuesAsNumpy()
		daily_precipitation_sum = daily.Variables(2).ValuesAsNumpy()
		daily_precipitation_hours = daily.Variables(3).ValuesAsNumpy()
		daily_precipitation_probability_max = daily.Variables(4).ValuesAsNumpy()
		daily_wind_speed_10m_max = daily.Variables(5).ValuesAsNumpy()
		daily_wind_direction_10m_dominant = daily.Variables(6).ValuesAsNumpy()

		daily_data = {"date": pd.date_range(
			start = pd.to_datetime(daily.Time(), unit = "s", utc = True),
			end = pd.to_datetime(daily.TimeEnd(), unit = "s", utc = True),
			freq = pd.Timedelta(seconds = daily.Interval()),
			inclusive = "left"
		)}
		
		daily_data["temperature_2m_max"] = daily_temperature_2m_max
		daily_data["temperature_2m_min"] = daily_temperature_2m_min
		daily_data["precipitation_sum"] = daily_precipitation_sum
		daily_data["precipitation_hours"] = daily_precipitation_hours
		daily_data["precipitation_probability_max"] = daily_precipitation_probability_max
		daily_data["wind_speed_10m_max"] = daily_wind_speed_10m_max
		daily_data["wind_direction_10m_dominant"] = daily_wind_direction_10m_dominant

		daily_dataframe = pd.DataFrame(data = daily_data)
		risultato_per_data = daily_dataframe.loc[daily_dataframe['date'].dt.date == data_richiesta.date()]
		return risultato_per_data

if __name__ == "__main__":
	meteo = Meteo()
	# Cattura la data attuale del sistema
	data_odierna = datetime.now()

	# Aggiunge un giorno alla data odierna per ottenere la data di domani
	data_domani = data_odierna + timedelta(days=0)

	# Formatta la data di domani nel formato desiderato (es. "AAAA-MM-GG")
	data_domani_str = data_domani.strftime("%Y-%m-%d")
	
	# print(meteo.info_previsioni(data_domani_str).at[1 ,'wind_direction_10m_dominant'])
	print(meteo.info_correnti())