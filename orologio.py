from PySide2.QtCore import  QDateTime,QTimer,QObject,QTime

class AggiornamentoOra(QObject):

    def __init__(self):
        super().__init__()

        self.timer = QTimer()
        self.timer.setInterval(1000) # aggiorna ogni secondo
        self.timer.timeout.connect(self.aggiorna_ora)

        self.ora_formattata = QTime().toString("hh:mm")

    def aggiorna_ora(self):
        ora = QDateTime.currentDateTime()
        self.ora_formattata = ora.time().toString("hh:mm")

    def get_ora_formattata(self):
        return self.ora_formattata